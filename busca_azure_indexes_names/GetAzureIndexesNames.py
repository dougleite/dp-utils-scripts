import requests
import json


headers = {'api-key': '9B2F404F9F187D9096BE658766FC4131', 'cache-control':'no-cache'}

def get_indexes_names():
	try:
		headers_get = {'api-key': '9B2F404F9F187D9096BE658766FC4131', 'cache-control':'no-cache'}
		url_get_indexes_names = 'https://dp-search-prd.search.windows.net/indexers/?api-version=2019-05-06&%24select=name'
		r = requests.get(url_get_indexes_names, headers=headers)
		return r
	except ValueError:
		print(ValueError)
	
def run_indexes(response):
    j = response.json()
    for item in j['value']:
        # para cada item encontrado executa o index na azure
		url_post_run = 'https://dp-search-prd.search.windows.net/indexers/' + item['name'] + '/run?api-version=2017-11-11' 
		print('Running index for %s' % (item['name']))
		try:
			r = requests.post(url_post_run, headers=headers)
		except ValueError:
			print(ValueError)
		

res = get_indexes_names()
run_indexes(res)		