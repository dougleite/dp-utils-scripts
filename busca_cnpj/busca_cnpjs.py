#!/usr/bin/env python
# coding: utf-8

# In[87]:


import requests
import csv
import pandas as pd
import numpy as np
from bs4 import BeautifulSoup


# In[88]:


#df = pd.read_excel("./operadoras.xlsx")
df = pd.read_excel('./operadoras.xlsx',sheetname='Planilha1',header=0,converters={'Nome':str,'Cod':str})


# In[89]:


def writeFile(lst):
    with open('./request_result.csv', 'w') as myfile:
        wr = csv.writer(myfile, quoting=csv.QUOTE_ALL)
        wr.writerow(lst)


# In[90]:


def getCnpj(registro_ans):
    try:
        r = requests.get("http://www.ans.gov.br/portal/site_novo/quali_consu/informacoes_operadora.asp?co_operadora_param=" + str(registro_ans))
        soup = BeautifulSoup(r.text, 'lxml')
        tr = soup.find_all('tr')
        return tr[1].select('td')[1].text, tr[2].select('td')[1].text, tr[3].select('td')[1].text
    except:
        pass


# In[91]:


cols = ['registro_ans', 'cnpj', 'razao_social']
lst = []

for cod in df['Cod']:
    print(str(cod))
    
    try:
        registro_ans, cnpj, razao_social = getCnpj(cod)
    except:
        pass
    lst.append([registro_ans, cnpj, razao_social])
df1 = pd.DataFrame(lst, columns=cols)
df1.to_excel("./output.xlsx")

#writeFile(request_RESULT)

