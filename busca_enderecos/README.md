**Buscar scripts por endereços via API**

Este script tem por objetivo buscar endereços utilizando CNPJs cadastrados na tabela cliente no banco de dados CORPORATE-PRD (Data Provider)
---

## Utilização

Abaixo seguem as dependências necessárias para a execução do script.

1. Fazer o clone deste repositório
2. Instalar as dependências para as libs pandas, requests, xlrd e openpyxl utilizando o comando pip install
3. Executar o script busca_endereco_cnpj.py

---

## Criação do arquivo de saída após o processamento.

Devido a limitações para utilização do serviço utilizado é possível apenas buscar 3 CNPJs por minuto, lógica já implementada no script.
Após a execução será gerado o arquivo output.xlsx contendo os endereços localizados.
